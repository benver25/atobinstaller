@echo off
cd > donchange.txt
set/p RootPath=<donchange.txt
if exist %RootPath%\atobinstaller rmdir /Q /S atobinstaller
del donchange.txt
mkdir atobinstaller
cd atobinstaller
cd > temp.txt
set /p WherePath=<temp.txt
echo Welcome to ATOB python libary installer. Wait a second to setup it. Press any key to start setuping installer.
pause
git clone https://github.com/BenVerdiman/atobinstaller.git
cd atobinstaller
copy whereispythonlib.py %WherePath%
cd %WherePath%
python whereispythonlib.py > path.txt
set /p PathLib=<path.txt
cd /d %PathLib%
if exist atob.py goto existfile
if NOT exist atob.py goto notexistfile

:existfile
echo ATOB libary is already exist on your computer.
set /p DeleteOrNo=Do you want to delete ATOB libary from your computer? (Y/N)
if %DeleteOrNo% == y (
	del atob.py
	echo All right! Press any key to close.
	cls
	cd/d %RootPath%
	rmdir /Q /S atobinstaller
	pause
	exit
)
if %DeleteOrNo% == Y (
	del atob.py
	echo All right! Press any key to close.
	cls
	cd/d %RootPath%
	rmdir /Q /S atobinstaller
	pause
	exit
)
if %DeleteOrNo% == n (
	echo All right! Press any key to close.
	cls
	cd/d %RootPath%
	rmdir /Q /S atobinstaller
	pause
	exit
)
if %DeleteOrNo% == N (
	echo All right! Press any key to close.
	cls
	cd/d %RootPath%
	rmdir /Q /S atobinstaller
	pause
)
exit /b
:notexistfile
git clone https://github.com/BenVerdiman/atoblibary
cd atoblibary
copy atob.py %PathLib%
cls
cd ..
rmdir /Q /S atoblibary
cls
cd /d %WherePath%
echo Installation is done. Wait a second to delete temp files.
cd ..
rmdir /Q /S atobinstaller
cls
echo Everything is done. Press any key to close.
pause
exit /b